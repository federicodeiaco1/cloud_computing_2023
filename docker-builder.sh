#!/bin/bash

# K8s docker engine
eval $(minikube -p minikube docker-env)

echo "Building images..."
# Doker build Catalog
docker build -t catalog-image ./Catalog/
docker build -t init-db-image ./Catalog/db/

# Docker build Users
docker build -t app-image ./Users/
docker build -t db-image ./Users/db/

# Docker build Analytics
docker build -t app-analytics-image ./Analytics/analytics
docker build -t analytics-sidecar-image ./Analytics/sidecar

# Docker build Order
docker build -t app-orders-image ./Order/

# Docker build Web
docker build -t web-image ./Web/

echo "Building images terminated"