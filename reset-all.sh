#!/bin/bash

kubectl delete all --all -n=default

kubectl delete secret --all

kubectl delete pvc --all
kubectl delete pv --all

kubectl delete ingress --all

minikube ssh "sudo rm -rf /mnt/data && exit"