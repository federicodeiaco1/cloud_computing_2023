from flask import Flask, request, jsonify, make_response
import redis
import json
import requests
from datetime import datetime
from random import randint
import os

def get_obj_by_id(uri):
    response = requests.get(uri)

    response = response.json()

    return response['result']

# Restituisce un JSON contenente un singolo ordine, con i relativi dettagli 
# (prelevati tramite request ad altri microservizi/db)
def get_order_by_id(order_id):

    order = db.get(order_id)
    order = json.loads(order)
    quantity = order['quantity']
    date = order['date']
    book_id = order['book_id']
    user_id = order['user_id']

    try:
        book = get_obj_by_id(os.getenv('BOOK_SERVICE_URL') + str(book_id))
        import sys
        print('book', book, file=sys.stderr)
    except Exception as e:
        import sys
        print('exception book', e, file=sys.stderr)
        return None
    try:
        user = get_obj_by_id(os.getenv('USER_SERVICE_URL') + str(user_id))
        import sys
        print('user', user, file=sys.stderr)
    except Exception as e:
        import sys
        print('exception user', e, file=sys.stderr)
        return None
    
    # Estrazione dettagli libro
    book_title = book['title']
    book_id = book['_id']
    book_authors = []
    for author in book['authors']:
        book_authors.append(author['name'])

    # Estrazione dettagli utente
    user_name = user['name']
    user_mail = user['email']

    # Creazione dizionario contente i dettagli dell'ordine
    order = {
        'order_id': order_id,
        'book_title': book_title,
        'book_id': book_id,
        'book_authors': book_authors,
        'user_name': user_name,
        'user_mail': user_mail,
        'quantity': quantity,
        'date': date
    }

    return order


DB_HOST = os.getenv('REDIS_URL')
DB_PORT = int(os.getenv('REDIS_PORT'))
DB_PASSWORD = os.getenv('REDIS_PASSWORD')


app = Flask(__name__)

db = redis.Redis(host=DB_HOST, port=DB_PORT, db=0, password=DB_PASSWORD)



@app.route('/orders', methods=['GET', 'POST'])
def orders_get_post():

    if request.method == 'GET':

        keys = db.keys()
        
        docs = []
        for k in keys:
            docs.append(get_order_by_id(k.decode('UTF-8')))

        response = {'response': docs}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 200
    
    elif request.method == 'POST':

        content = request.json
        book_id = content['book_id']
        user_id = content['user_id']
        quantity = content['quantity']
        date = datetime.today().strftime('%d-%m-%Y')
        order_id =  str(book_id) + str(user_id) + str(randint(1, 999))
        order_id = str(hash(order_id))

        order = {
            'book_id': book_id,
            'user_id': user_id,
            'quantity': quantity,
            'date': date
        }

        order = json.dumps(order)

        db.set(order_id, order)
    
        response = {'response': 'Order added'}
        response = jsonify(response)
        response.headers["Content-Type"] = "application/json"
        return response, 200



@app.route('/order/<id>', methods=['GET', 'DELETE'])
def single_order_get_post(id):

    if request.method == 'GET':

        try:
            order = get_order_by_id(id)

            # Nel caso in cui non venga trovato il libro o l'utente
            # (Libro o utente eliminato)
            if order == None:
                response = {'response': 'Error, book or user not found'}
                response = make_response(jsonify(response,404))
                response.headers["Content-Type"] = "application/json"
                return response

            # Nel caso in cui sia tutto ok
            response = {'response': order}
            response = jsonify(response)
            response.headers["Content-Type"] = "application/json"
            return response, 200

        # Nel caso in cui non venga trovato l'id dell'ordine
        except:
            response = {'response': 'Error, order not found'}
            response = jsonify(response)
            response.headers["Content-Type"] = "application/json"
            return response, 404

    if request.method == 'DELETE':
        
        # Nel caso in cui sia tutto ok 
        try:
            db.delete(id)
            response = {'response': 'Order deleted'}
            response = jsonify(response)
            response.headers["Content-Type"] = "application/json"
            return response, 200

        # Nel caso in cui non venga trovato l'id dell'ordine
        except:
            response = {'response': 'Error, order not found'}
            response = jsonify(response)
            response.headers["Content-Type"] = "application/json"
            return response, 404


if __name__ == '__main__':
    app.run(host="0.0.0.0", port = int(os.getenv('PORT')))