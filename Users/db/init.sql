CREATE DATABASE userdb;

USE userdb;

CREATE TABLE Users (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    Name VARCHAR(255) NOT NULL, 
    Email VARCHAR(255) NOT NULL,
    Psw VARCHAR(255) NOT NULL,
    UNIQUE (Email)
    );

INSERT INTO Users VALUES (1, 'admin', 'admin@admin.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');