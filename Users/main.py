from flask import Flask, request
from flask_cors import CORS
import threading
import mysql.connector
import requests
import os
import json
from jsonschema import validate, ValidationError, SchemaError

def validate_json(json_to_validate, schema_file):
    with open(schema_file) as json_file:
        schema = json.load(json_file)
    json_file.close()
    try:
        validate(instance=json_to_validate, schema=schema)
        return True
    except ValidationError:
        return False
    except SchemaError:
        return False

app = Flask(__name__)
CORS(app)

def connect_to_db():
    mydb = mysql.connector.connect(
    host=os.getenv("DB_ADDRESS"),
    port=os.getenv("DB_PORT"),
    user=os.getenv("MYSQL_USER"),
    password=str(os.getenv("MYSQL_PASSWORD")),
    database=os.getenv("MYSQL_DB")
    )

    mycursor = mydb.cursor()

    return (mydb, mycursor)

def close_connection(mydb, mycursor):
    mycursor.close()
    mydb.close()


def flask_thread():
    app.run(host="0.0.0.0", port=int(os.getenv('PORT')))

# Leggere
@app.route("/user/<id>", methods=["GET"])
def get(id):
    mydb, mycursor = connect_to_db()

    sql = "SELECT * FROM Users WHERE id = " + str(id)
    mycursor.execute(sql)

    myresult = mycursor.fetchall()
    if myresult:
        res = dict()
        res["id"] = myresult[0][0]
        res["name"] = myresult[0][1]
        res["email"] = myresult[0][2]
        close_connection(mydb, mycursor)
        return get_response("ok", res)
    else:
        close_connection(mydb, mycursor)
        return get_response("error", "no data")

# Aggiungere utenti
@app.route("/user", methods=["POST"])
def insert():
    data = request.get_json()
    if not validate_json(data, schema_file="user_schema.json"):
        return get_response("error", "invalid user schema")
    if data:
        response = requests.get("http://api.eva.pingutil.com/email?email=" + data["email"])
    else:
        return get_response("error", "no data")
    mail_eva_response = response.json()
    if mail_eva_response["data"]["valid_syntax"]:
        mydb, mycursor = connect_to_db()

        sql = "INSERT INTO Users (Name, Email, Psw) VALUES (%s, %s, %s)"
        val = (data["name"], data["email"], data["psw"])
        try:
            mycursor.execute(sql, val)
            mydb.commit()
        except mysql.connector.errors.IntegrityError:
            close_connection(mydb, mycursor) 
            return get_response("error", "mail already exists")
        close_connection(mydb, mycursor)
        return get_response("ok", str(mycursor.rowcount) + " record inserted.")
    else:
        return get_response("error", "invalid mail syntax")

@app.route("/login_user", methods=["POST"])
def login_user():
    data = request.get_json()
    if not validate_json(data, schema_file="user_login_schema.json"):
        return get_response("error", "invalid user login schema")
    mydb, mycursor = connect_to_db()

    sql = "SELECT id, Name FROM Users WHERE Email LIKE '" + str(data["email"]) + "' AND Psw LIKE '" + str(data["psw"]) + "'"
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    if myresult:
        res = dict()
        res["id"] = myresult[0][0]
        res["name"] = myresult[0][1]
        close_connection(mydb, mycursor)
        return get_response("ok", res)
    else:
        close_connection(mydb, mycursor)
        return get_response("error", "no data")

def get_response(status, res = None):
    return {"status" : status, "result" : res}

def start_server():
    thread = threading.Thread(target=flask_thread)
    thread.start()

if __name__ == "__main__":
    thread = threading.Thread(target=flask_thread)
    thread.start()
