#!/bin/bash

echo "K8s apply ..."
# K8s apply Catalog
kubectl apply -f ./Catalog/kubernetes/

#K8s apply Users
kubectl apply -f ./Users/kubernetes/

#K8s apply Order
kubectl apply -f ./Order/kubernetes/

#K8s apply Analytics
kubectl apply -f ./Analytics/kubernetes/

#K8s apply Web
kubectl apply -f ./Web/kubernetes/

#K8s apply API gateway
kubectl apply -f ./API_Gateway/

echo "K8s apply terminated"