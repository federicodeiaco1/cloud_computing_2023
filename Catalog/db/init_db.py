import requests
import pymongo
import os


print("Init database")
DATABASE_URL = os.getenv('DB_CONNECTION')
client = pymongo.MongoClient("mongodb://" + 
                            str(os.getenv('MONGO_USERNAME')) +
                            ':' + str(os.getenv('MONGO_PASSWORD')) +
                            '@' + str(DATABASE_URL))

db = client['DATABASE']

try:
    collection = db.create_collection('Books')
except pymongo.errors.CollectionInvalid:
    collection = db.get_collection('Books')


#726 -> 23.2k -> 10min
#363 -> 11.6k -> 5min
page_stop = 4
page = 1
response = requests.get('https://gutendex.com/books/?page=1')
response = response.json()

for p in range(page_stop):
    for i in response['results']:
        book = dict()
        book['_id'] = i['id']
        book['title'] = i['title']
        book['authors'] = i['authors']
        book['languages'] = i['languages']
        try:
            book['cover'] = i['formats']['image/jpeg']
        except:
            book['cover'] = None
        try:
            collection.insert_one(book)
        except:
            pass
    print("Saved page " + str(page))
    response = requests.get(response['next'])
    response = response.json()
    page += 1

print("Done!")