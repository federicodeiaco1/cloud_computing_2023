from flask import Flask, jsonify
import pymongo
import os

app = Flask(__name__)

def db_connection():
	DATABASE_URL = os.getenv('DB_CONNECTION')
	client = pymongo.MongoClient("mongodb://" + 
								str(os.getenv('MONGO_USERNAME')) +
								':' + str(os.getenv('MONGO_PASSWORD')) +
								'@' + str(DATABASE_URL))
	db = client['DATABASE']

	try:
		collection = db.create_collection('Books')
	except pymongo.errors.CollectionInvalid:
		collection = db.get_collection('Books')
	return collection


def get_response(status, res = None):
    return jsonify({"status" : status, "result" : res})

@app.route('/books',  methods=['GET'])
def get_books():
	res = db_conn.find({})
	res_list = list(res)
	if res:
		return get_response("ok", res_list)
	return get_response("error")

@app.route('/books/page/<page>',  methods=['GET'])
def get_books_per_page(page):
	limit_page = int(8)
	res = db_conn.find({}).skip((int(page)-1)*limit_page).limit(limit_page)
	res_list = list(res)
	if res:
		return get_response("ok", res_list)
	return get_response("error")

@app.route('/books/<book_id>',  methods=['GET'])
def get_books_by_id(book_id):
	res = db_conn.find_one({'_id': int(book_id)})
	if res:
		return get_response("ok", res)
	return get_response("not found")



if __name__ == "__main__":
	global db_conn
	db_conn = db_connection()
	app.run(host='0.0.0.0', port=int(os.environ.get('PORT')))