# Cloud_Computing_2023

Repository &rarr; https://gitlab.com/federicodeiaco1/cloud_computing_2023

## Members

### Group 1
    - Emanuele Albarino 889618
    - Federico De Iaco 845587
### Group 2
    - Luigi Avenoso 844787
    - Federico Bergamini 845646

The project is about an online library. There are two main interfaces: Web and API Gateway.

## External API Services
- EVA: https://eva.pingutil.com/
- Quick Chart: https://quickchart.io/
- Gutendex : https://gutendex.com/

## Patterns
- Initialization pattern: used for initializing the Catalog service database.
- Side-car pattern: used in the Analytics service in order to have analytics about orders.

## Project Schema
![alt text](Progetto_CC.png "Project Schema")

## Build and run locally

1. Connect to your virtual machine.
    - Be sure you forward the address 192.168.49.2:80 to the local address *localhost:8080*.
    - example `ssh -L 8080:192.168.49.2:80 ...`
2. On your virtual machine, clone the repository and get inside the project folder.
    - `git clone https://gitlab.com/federicodeiaco1/cloud_computing_2023.git `
    - `cd cloud_computing_2023/`
2. On your virtual machine, type the istruction:
    - `minkube start`
    - `minikube addons enable ingress`
3. On your virtual machine, Build the images (this will take a while). 
    - `chmod +x ./docker-builder.sh`
    - `./docker-builder.sh`
4. On your virtual machine, Launch kubernetes manifests.
    - `chmod +x ./k8s-launcher.sh`
    - `./k8s-launcher.sh`
5. on **your computer**, Add the following lines **at the end** of the file `/etc/hosts`.
    - `127.0.0.1       api.library.com`
    - `127.0.0.1       web.library.com`
6. Head to [the main page](web.library.com:8080).
7. Enjoy 😃:
    1. Click on *sign up*.
    2. Fill the form.
        - Try a syntactically incorrect mail to test the *EVA* third party service.
    3. Login with the info provided in the previous step.
    4. Try to order some books (al least two).
    5. Logout.
    6. Login with:
        - username : *admin@admin.com*
        - password : *password*
    7. Get the analytics for the books you just ordered (left part) (*Quick Chart*)
    8. Get the analytics for the current date (that depends on the time you are running the service) (*Quick Chart*)
