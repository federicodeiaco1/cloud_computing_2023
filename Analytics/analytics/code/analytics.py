import os, sys, pymongo
from flask import Flask, jsonify, request
from datetime import datetime
from PIL import Image
from quickchart import QuickChart

MONGO_URL=os.environ.get('MONGO_URL')

client = pymongo.MongoClient("mongodb://" + 
                            str(os.getenv('MONGO_USERNAME')) +
                            ':' + str(os.getenv('MONGO_PASSWORD')) +
                            '@' + str(MONGO_URL))

db = client['DATABASE']
collection_name = 'Analytics'

try:
    collection = db.create_collection(collection_name)
except pymongo.errors.CollectionInvalid:
    collection = db.get_collection(collection_name)

app = Flask(__name__)

@app.route('/analytics/daily_report/<date>', methods=['GET'])
def daily_report(date):
    #date = datetime.strptime(date, '%d-%m-%Y')

    cursor = collection.find({
        'date' : date
    })

    response = {
        'date' : date,
        'counts' : []
    }

    for document in cursor:
        response['counts'].append(
            {
                'book_id' : document['book_id'],
                'quantity' : document['quantity']
            }
        ) 
    
    labels = [document['book_id'] for document in response['counts']]
    data = [document['quantity'] for document in response['counts']]

    print(len(labels), len(data), file=sys.stderr)
    
    qc = QuickChart()
    qc.config = {
        "type": "bar",
        "data": {
            "labels": labels,
            "datasets": [{
                "label": response['date'],
                "data": data
            }]
        }
    }

    response['chart_url'] = qc.get_short_url()

    return jsonify(response)

@app.route('/analytics/book_report/<book_id>', methods=['GET'])
def book_report(book_id):
    cursor = collection.find({
        'book_id' : int(book_id)
    })

    response = {
        'book_id' : book_id,
        'daily_requests' : []
    }

    for document in cursor:
        response['daily_requests'].append(
            {
                'date' : document['date'],
                'quantity' : document['quantity']
            }
        )
    
    dates = [req ['date'] for req in response['daily_requests']]
    qty = [req ['quantity'] for req in response['daily_requests']]
    
    qc = QuickChart()
    qc.config = {
        "type": "bar",
        "data": {
            "labels": dates,
            "datasets": [{
                "label": response['book_id'],
                "data": qty
            }]
        }
    }

    response['chart_url'] = qc.get_short_url()

    print(response, file=sys.stderr)

    return jsonify(response)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port = int(os.environ['PORT']), debug = True)