import os, sys, requests
import pymongo
from datetime import datetime

MONGO_URL=os.environ.get('MONGO_URL')

client = pymongo.MongoClient("mongodb://" + 
                            str(os.getenv('MONGO_USERNAME')) +
                            ':' + str(os.getenv('MONGO_PASSWORD')) +
                            '@' + str(MONGO_URL))

db = client['DATABASE']
collection_name = 'Analytics'

try:
    collection = db.create_collection(collection_name)
except pymongo.errors.CollectionInvalid:
    collection = db.get_collection(collection_name)


BASE_URL = os.environ.get('BASE_URL')

def main():
    response = requests.get(BASE_URL)
    response = response.json()
    response = response['response']

    print('response:', response, file=sys.stderr)

    clean_dataset = []

    for document in response:
        item = {
            'book_id' : document['book_id'],
            'quantity' : 0,
            'date' : document['date'],
        }
        if item not in clean_dataset:
            clean_dataset.append(item) 

    for document in response:
        for item in clean_dataset:
            if item['book_id'] == document['book_id'] and item['date'] == document['date']:
                item['quantity'] += document['quantity']
    
    print('clean_dataset:', clean_dataset, file=sys.stderr)

    collection.delete_many({})
    collection.insert_many(clean_dataset)


if __name__ == '__main__':
    main()