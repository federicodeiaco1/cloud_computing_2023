from flask import Flask, render_template, request, session, redirect, url_for, flash, jsonify
from flask_paginate import Pagination
import requests
import os
from hashlib import sha256


app = Flask(__name__)
app.secret_key = os.getenv('FLASK_SECRET')


@app.route('/',  methods=['GET'])
def index():
	if "user" in session:
		page = 1
		if request.args:
			page = request.args.get('page', type=int, default=1)

		b_catalog = requests.get(os.getenv('BOOK_SERVICE_URL')).json()['result']

		response = requests.get(os.getenv('BOOK_SERVICE_URL') + '/page/' + str(page))
		books = response.json()['result']

		pagination = Pagination(page=page, total=len(b_catalog), per_page = 8, record_name='books')
		
		return render_template("index.html", active_user=session['user'], books=books, pagination=pagination)
	return render_template("index.html", active_user=None)


@app.route('/login',  methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		form_data = request.form
		data = {
			"email": form_data['email'],
		    "psw": sha256(form_data['psw'].encode('utf-8')).hexdigest()
		}
		res = requests.post(os.getenv('USER_SERVICE_URL') + '/login_user', json=data)
		res = res.json()

		if res['status'] == "ok":
			session["user"] = (int(res['result']['id']), res['result']['name'])
			flash("Logged in!", "alert alert-success")
			if int(session["user"][0]) == 1:
				return redirect(url_for("analytics")) 
			return redirect(url_for("index"))
		else:
			flash("Mail or password are wrong", "alert alert-danger")
			return redirect(url_for("login"))
	elif "user" in session:
		return redirect(url_for("index"))
	return render_template("login.html")


@app.route('/signup',  methods=['GET', 'POST'])
def signup():
	if request.method == 'POST':
		form_data = request.form
		data = {
			"name": form_data['name'],
			"email": form_data['email'],
		    "psw": sha256(form_data['psw'].encode('utf-8')).hexdigest()
		}
		res = requests.post(os.getenv('USER_SERVICE_URL') + '/user', json=data)
		res = res.json()
		
		if res['status'] == "ok":
			flash("Signed up", "alert alert-success")
		else:
			flash("An error occurred", "alert alert-danger")
		return redirect(url_for("index"))
	elif "user" in session:
		return redirect(url_for("index"))
	return render_template("signup.html")


@app.route('/bookdetails/<book_id>',  methods=['GET', 'POST'])
def bookdetails(book_id):
	if "user" in session:
		if request.method == 'POST':
			form_data = request.form
			data = {
				"book_id": int(book_id),
				"user_id": int(session["user"][0]),
			    "quantity": int(form_data['quantity'])
				}
			# Chiamata endopoint order
			res = requests.post(os.getenv('ORDER_SERVICE_URL'), json=data)
			if res:
				flash("Order added", "alert alert-success")
			else:
				flash("Error, order not added", "alert alert-danger")
			return redirect(url_for('index'))
		

		response = requests.get(os.getenv('BOOK_SERVICE_URL') + '/' + str(book_id))
		response = response.json()

		if response['status'] == 'ok':
			return render_template("bookdetails.html", data=response['result'])
		else:
			flash("Book not found", "alert alert-danger")
	return redirect(url_for("index"))

@app.route("/analytics", methods=["GET"])
def analytics():
	if int(session["user"][0]) == 1:
		bookid = request.args.get('bookid')
		date = request.args.get('date')
		if (bookid is None) and (date is None):
			return render_template("home_analytics.html", active_user=session["user"])
		elif bookid is not None:
			res = requests.get(os.getenv('ANALYTICS_SERVICE_URL') + '/book_report/' + str(bookid))
			res = res.json()
			return render_template("home_analytics.html", active_user=session["user"], result_url=res["chart_url"])
		else:
			dd = date.split("-")
			date = dd[2] + "-" + dd[1] + "-" + dd[0]
			res = requests.get(os.getenv('ANALYTICS_SERVICE_URL') + '/daily_report/' + str(date))
			res = res.json()
			return render_template("home_analytics.html", active_user=session["user"], result_url=res["chart_url"])
	flash("Restricted area", "alert alert-danger")
	return redirect(url_for("index"))



@app.route("/logout")
def logout():
    session.pop("user", None)
    return redirect(url_for("index"))


if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(os.environ.get('PORT')))